// Prevent uibuilder not defined warning
/*if (typeof uibuilder === "undefined") {
    var uibuilder = null;
}*/

export default {
    template: `
        <!-- <div style="border: 1px solid silver;">
            <p>Ooh, my very own Vue component! No build step was required. 😁</p>
            <p>
                Pressing the button not only increments the counter but also sends the
                data back to Node-RED.
            </p>
            <p>A component counter: {{ count }} <button data-value="0" @click="doEvent">Count</button></p>
        </div> -->
        <table>
            <tr>
                <td>Puzzle</td>
                <td>Solved</td>
                <td>Admin solve</td>
                <td>Admin reset</td>
            </tr>
            <tr v-for="[index, state]  in puzzleStates.entries()">
                <td>{{this.puzzleNames[index] ?? index}}</td>
                <td>{{state}}</td>
                <td><button type="button" @click="this.solve(index)">SOLVE</button></td>
                <td><button type="button" @click="this.reset(index)">RESET</button></td>
            </tr>
        </table>
        <form id="hintform">
            <label for="hint">Show Hint:</label>
            <input type="text" name="hint" id="hint" v-model="hintText">
            <input type="submit" value="Submit">
            <input type="button" @click="this.clearHint()" value="Clear">
        </form>
        <p><button type="button" @click="this.setLang('de')">Set DE</button> <button type="button" @click="this.setLang('en')">Set EN</button></p>
        <p><button type="button" @click="this.playPortalWarning()">PLAY PORTAL WARNING</button></p>
        <p><button type="button" @click="this.playPortalCurtainHint()">PLAY PORTAL CURTAIN HINT</button></p>
        <p><button type="button" @click="this.resetAllPuzzles()">RESET ALL PUZZLES</button></p>
        <p><button type="button" @click="this.lightOn()">LIGHT ON</button> <button type="button" @click="this.lightOff()">LIGHT OFF</button></p>
        <p><button type="button" @click="this.forestLightOn(true)">FOREST LIGHT ON</button> <button type="button" @click="this.forestLightOn(false)">FOREST LIGHT OFF</button></p>
        <p><button type="button" @click="this.witchLightOn(true)">WITCH LIGHT ON</button> <button type="button" @click="this.witchLightOn(false)">WITCH LIGHT OFF</button></p>
        <p><button type="button" @click="this.witchEmergencyDoorOpen()">WITCH EMERGENCY DOOR OPEN</button> <button type="button" @click="this.witchEmergencyDoorClose()">WITCH EMERGENCY DOOR CLOSE</button></p>
        <p><button type="button" @click="this.endDoorOpen()">END DOOR OPEN</button> <button type="button" @click="this.endDoorClose()">END DOOR CLOSE</button></p>
        <p><button type="button" @click="this.dwarfCaseOpen()">Dwarf Case OPEN</button> <button type="button" @click="this.dwarfCaseClose()">Dwarf Case CLOSE</button></p>
        <p><button type="button" @click="this.witchToPortalOpen()">WITCH <-> PORTAL</button> <button type="button" @click="this.portalToDwarfsOpen()">PORTAL <-> DWARFS</button></p>
        <p><button type="button" @click="this.setHintBackground(0)">Witch Hint Background</button> <button type="button" @click="this.setHintBackground(1)">Dwarf Hint Background</button></p>
        <p><button type="button" @click="this.stopGrammophone()">Stop Grammophone</button></p>
    `,

    data() {
        let puzzleNames = [
            "Entry",
            "Startgame",
            "Hexenhaus",
            "Klavier",
            "Tarot",
            "Geruch",
            "Trank",
            "Kamin",
            "Portal",
            "Betten",
            "Grammophon",
            "Tisch",
            "Spiegel",
            "Säulen (Ende)"
        ];
        let puzzleStates = Array(puzzleNames.length).fill(0);

        return { 
            count: 0,
            puzzleStates: puzzleStates,
            puzzleNames: puzzleNames,
            hintText: ""
        }
    },

    // Supporting functions
    methods: {

        // Use the uib helper function to send something to NR
        doEvent(event) {
            // Set the data-value attribute to the new count
            // so that it is sent back to node-red in the msg.payload
            event.target.dataset.value = ++this.count
            
            uibuilder.eventSend(event)
        },

        getImageUrl(state) {
            return state ? "check.png" : "decline.png";
        },

        solve(puzzleIndex) {
            let puzzleName = this.puzzleNames[puzzleIndex];
            if (confirm(`Solve puzzle "${puzzleName}"?`)) {
                uibuilder.send({
                    "topic": "adminsolve",
                    "puzzle": puzzleIndex
                });
            }
        },

        reset(puzzleIndex) {
            let puzzleName = this.puzzleNames[puzzleIndex];
            if (confirm(`Reset puzzle "${puzzleName}"?`)) {
                uibuilder.send({
                    "topic": "adminreset",
                    "puzzle": puzzleIndex
                });
            }
        },

        resetAllPuzzles() {
            if (confirm("Reset all puzzles?")) {
                uibuilder.send({
                    "topic": "adminreset"
                });
            }
        },

        lightOn() {
            if (confirm("Turn light on?")) {
                uibuilder.send({
                    "topic": "lighton"
                });
            }
        },

        lightOff() {
            if (confirm("Turn light off?")) {
                uibuilder.send({
                    "topic": "lightoff"
                });
            }
        },

        forestLightOn(value) {
            if (confirm(`Turn forest light ${value ? "on" : "off"}?`)) {
                uibuilder.send({
                    "topic": "forestLight",
                    "payload": value
                });
            }
        },

        witchLightOn(value) {
            if (confirm(`Turn witch light ${value ? "on" : "off"}?`)) {
                uibuilder.send({
                    "topic": "witchLight",
                    "payload": value
                });
            }
        },

        showHint(hint) {
            uibuilder.send({
                "topic": "showhint",
                "payload": hint
            });
        },

        witchEmergencyDoorOpen() {
            if (confirm("Open witch emergency door?")) {
                uibuilder.send({
                    "topic": "witchEmergencyDoorOpen",
                });
            }
        },

        witchEmergencyDoorClose() {
            if (confirm("Close witch emergency door?")) {
                uibuilder.send({
                    "topic": "witchEmergencyDoorClose",
                });
            }
        },

        endDoorOpen() {
            if (confirm("Open end door?")) {
                uibuilder.send({
                    "topic": "endDoorOpen",
                });
            }
        },

        endDoorClose() {
            if (confirm("Close end door?")) {
                uibuilder.send({
                    "topic": "endDoorClose",
                });
            }
        },

        dwarfCaseOpen() {
            if (confirm("Open Dwarf Case?")) {
                uibuilder.send({
                    "topic": "dwarfCaseOpen",
                });
            }
        },

        dwarfCaseClose() {
            if (confirm("Close Dwarf Case?")) {
                uibuilder.send({
                    "topic": "dwarfCaseClose",
                });
            }
        },

        witchToPortalOpen() {
            if (confirm("Open gate from witch to portal room?")) {
                uibuilder.send({
                    "topic": "witchToPortalOpen",
                });
            }
        },

        portalToDwarfsOpen() {
            if (confirm("Open gate from portal to dwarfs room?")) {
                uibuilder.send({
                    "topic": "portalToDwarfsOpen",
                });
            }
        },

        clearHint() {
            this.hintText = "";
            this.showHint(this.hintText);
        },

        playPortalWarning() {
            if (confirm("Play portal warning?")) {
                uibuilder.send({
                    "topic": "playPortalWarning"
                });
            }
        },

        playPortalCurtainHint() {
            if (confirm("Play portal curtain hint?")) {
                uibuilder.send({
                    "topic": "playCurtainHint"
                });
            }
        },

        setLang(lang) {
            if (confirm(`Set game language to ${lang.toUpperCase()}?`)) {
                uibuilder.send({
                    "topic": "setLang",
                    "payload": lang
                });
            }
        },

        setHintBackground(bg) {
            let bgName = bg ? "dwarf" : "witch";
            if (confirm(`Set ${bgName} hint background?`)) {
                uibuilder.send({
                    "topic": "setHintBackground",
                    "payload": bg
                });
            }
        },

        stopGrammophone() {
            if (confirm("Stop the grammophone music?")) {
                uibuilder.send({
                    "topic": "stopGrammophone"
                });
            }
        }
    },

    mounted() {
        // If msg changes - msg is updated when a standard msg is received from Node-RED
        uibuilder.onChange('msg', (msg) => {
            console.log('>> msg recvd >>', msg, this)

            // If the msg.payload is a string, show in on the page
            if (msg.topic == "puzzleStates") {
                this.puzzleStates = msg.payload;
            }
        });

        document.getElementById("hintform").addEventListener("submit", (e) => {
            e.preventDefault();
            this.showHint(this.hintText);
        });
    }
}
